YOURLS Bot
==========

.. image:: https://img.shields.io/badge/User-Guide-orange?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgdmlld0JveD0iLTEgLTI1NiAxNjY1LjMyODIgMTUzNiIKICAgaWQ9InN2ZzMwMjUiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSIKICAgd2lkdGg9IjE2NjUuMzI4MiIKICAgaGVpZ2h0PSIxNTM2IgogICBzb2RpcG9kaTpkb2NuYW1lPSJCb29rX2ZvbnRfYXdlc29tZS5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSGlucmljaFxEZXNrdG9wXEJvb2tfZm9udF9hd2Vzb21lLnBuZyIKICAgaW5rc2NhcGU6ZXhwb3J0LXhkcGk9IjUuNzYwMDAwMiIKICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9IjUuNzYwMDAwMiI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMzAzNSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczMwMzMiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxODAwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwNTkiCiAgICAgaWQ9Im5hbWVkdmlldzMwMzEiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDkzMTIzNDM4IgogICAgIGlua3NjYXBlOmN4PSI5MDguNDE4NzIiCiAgICAgaW5rc2NhcGU6Y3k9IjcxNC4xMzg4NCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTkiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii05IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMzAyNSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOmN1cnJlbnRDb2xvciIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlkPSJwYXRoMzAyOSIKICAgICBkPSJtIDE2MzguNTIxNyw5NCBxIDQwLDU3IDE4LDEyOSBsIC0yNzUsOTA2IHEgLTE5LDY0IC03Ni41LDEwNy41IC01Ny41LDQzLjUgLTEyMi41LDQzLjUgSCAyNTkuNTIxNzQgcSAtNzcsMCAtMTQ4LjUsLTUzLjUgUSAzOS41MjE3MzksMTE3MyAxMS41MjE3MzksMTA5NSBxIC0yNCwtNjcgLTEuOTk5OTk5OSwtMTI3IDAsLTQgMi45OTk5OTk5LC0yNyAzLC0yMyA0LC0zNyAxLC04IC0zLC0yMS41IC0zLjk5OTk5OTksLTEzLjUgLTMsLTE5LjUgMiwtMTEgOCwtMjEgNiwtMTAgMTYuNSwtMjMuNSAxMC41LC0xMy41IDE2LjUsLTIzLjUgMjMsLTM4IDQ1LC05MS41IDIyLjAwMDAwMSwtNTMuNSAzMC4wMDAwMDEsLTkxLjUgMywtMTAgMC41LC0zMCAtMi41LC0yMCAtMC41LC0yOCAzLC0xMSAxNywtMjggMTQsLTE3IDE3LC0yMyAyMSwtMzYgNDIsLTkyIDIxLC01NiAyNSwtOTAgMSwtOSAtMi41LC0zMiAtMy41LC0yMyAwLjUsLTI4IDQsLTEzIDIyLC0zMC41IDE4LC0xNy41IDIyLC0yMi41IDE5LC0yNiA0Mi41LC04NC41IDIzLjUsLTU4LjUgMjcuNSwtOTYuNSAxLC04IC0zLC0yNS41IC00LC0xNy41IC0yLC0yNi41IDIsLTggOSwtMTggNywtMTAgMTgsLTIzIDExLC0xMyAxNywtMjEgOCwtMTIgMTYuNSwtMzAuNSA4LjUsLTE4LjUgMTUsLTM1IDYuNSwtMTYuNSAxNiwtMzYgOS41LC0xOS41IDE5LjUsLTMyIDEwLC0xMi41IDI2LjUsLTIzLjUgMTYuNSwtMTEgMzYsLTExLjUgMTkuNSwtMC41IDQ3LjUsNS41IGwgLTEsMyBxIDM4LC05IDUxLC05IGggNzYwLjk5OTk2IHEgNzQsMCAxMTQsNTYgNDAsNTYgMTgsMTMwIGwgLTI3NCw5MDYgcSAtMzYsMTE5IC03MS41LDE1My41IC0zNS41LDM0LjUgLTEyOC41LDM0LjUgSCAxNTUuNTIxNzQgcSAtMjcsMCAtMzgsMTUgLTExLDE2IC0xLDQzIDI0LDcwIDE0NCw3MCBoIDkyMi45OTk5NiBxIDI5LDAgNTYsLTE1LjUgMjcsLTE1LjUgMzUsLTQxLjUgbCAzMDAsLTk4NyBxIDcsLTIyIDUsLTU3IDM4LDE1IDU5LDQzIHogTSA1NzQuNTIxNzQsOTYgcSAtNCwxMyAyLDIyLjUgNiw5LjUgMjAsOS41IGggNjA3Ljk5OTk2IHEgMTMsMCAyNS41LC05LjUgMTIuNSwtOS41IDE2LjUsLTIyLjUgbCAyMSwtNjQgcSA0LC0xMyAtMiwtMjIuNSAtNiwtOS41IC0yMCwtOS41IEggNjM3LjUyMTc0IHEgLTEzLDAgLTI1LjUsOS41IC0xMi41LDkuNSAtMTYuNSwyMi41IHogbSAtODMsMjU2IHEgLTQsMTMgMiwyMi41IDYsOS41IDIwLDkuNSBoIDYwNy45OTk5NiBxIDEzLDAgMjUuNSwtOS41IDEyLjUsLTkuNSAxNi41LC0yMi41IGwgMjEsLTY0IHEgNCwtMTMgLTIsLTIyLjUgLTYsLTkuNSAtMjAsLTkuNSBIIDU1NC41MjE3NCBxIC0xMywwIC0yNS41LDkuNSAtMTIuNSw5LjUgLTE2LjUsMjIuNSB6IiAvPgo8L3N2Zz4K
   :target: https://hirschheissich.gitlab.io/yourls-bot/userguide.html
   :alt: User Guide

.. image:: https://img.shields.io/badge/python-3.8+-blue?logo=python&logoColor=white
   :target: https://www.python.org/doc/versions/
   :alt: Supported Python versions

.. image:: https://img.shields.io/badge/backend-python--telegram--bot-blue
   :target: https://python-telegram-bot.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/frontend%20for-YOURLS-green
   :target: https://yourls.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/documentation-is%20here-orange?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcKICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIgogICB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiCiAgIHhtbG5zOnN2Zz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciCiAgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIKICAgeG1sbnM6c29kaXBvZGk9Imh0dHA6Ly9zb2RpcG9kaS5zb3VyY2Vmb3JnZS5uZXQvRFREL3NvZGlwb2RpLTAuZHRkIgogICB4bWxuczppbmtzY2FwZT0iaHR0cDovL3d3dy5pbmtzY2FwZS5vcmcvbmFtZXNwYWNlcy9pbmtzY2FwZSIKICAgdmlld0JveD0iLTEgLTI1NiAxNjY1LjMyODIgMTUzNiIKICAgaWQ9InN2ZzMwMjUiCiAgIHZlcnNpb249IjEuMSIKICAgaW5rc2NhcGU6dmVyc2lvbj0iMC45Mi40ICg1ZGE2ODljMzEzLCAyMDE5LTAxLTE0KSIKICAgd2lkdGg9IjE2NjUuMzI4MiIKICAgaGVpZ2h0PSIxNTM2IgogICBzb2RpcG9kaTpkb2NuYW1lPSJCb29rX2ZvbnRfYXdlc29tZS5zdmciCiAgIGlua3NjYXBlOmV4cG9ydC1maWxlbmFtZT0iQzpcVXNlcnNcSGlucmljaFxEZXNrdG9wXEJvb2tfZm9udF9hd2Vzb21lLnBuZyIKICAgaW5rc2NhcGU6ZXhwb3J0LXhkcGk9IjUuNzYwMDAwMiIKICAgaW5rc2NhcGU6ZXhwb3J0LXlkcGk9IjUuNzYwMDAwMiI+CiAgPG1ldGFkYXRhCiAgICAgaWQ9Im1ldGFkYXRhMzAzNSI+CiAgICA8cmRmOlJERj4KICAgICAgPGNjOldvcmsKICAgICAgICAgcmRmOmFib3V0PSIiPgogICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2Uvc3ZnK3htbDwvZGM6Zm9ybWF0PgogICAgICAgIDxkYzp0eXBlCiAgICAgICAgICAgcmRmOnJlc291cmNlPSJodHRwOi8vcHVybC5vcmcvZGMvZGNtaXR5cGUvU3RpbGxJbWFnZSIgLz4KICAgICAgPC9jYzpXb3JrPgogICAgPC9yZGY6UkRGPgogIDwvbWV0YWRhdGE+CiAgPGRlZnMKICAgICBpZD0iZGVmczMwMzMiIC8+CiAgPHNvZGlwb2RpOm5hbWVkdmlldwogICAgIHBhZ2Vjb2xvcj0iI2ZmZmZmZiIKICAgICBib3JkZXJjb2xvcj0iIzY2NjY2NiIKICAgICBib3JkZXJvcGFjaXR5PSIxIgogICAgIG9iamVjdHRvbGVyYW5jZT0iMTAiCiAgICAgZ3JpZHRvbGVyYW5jZT0iMTAiCiAgICAgZ3VpZGV0b2xlcmFuY2U9IjEwIgogICAgIGlua3NjYXBlOnBhZ2VvcGFjaXR5PSIwIgogICAgIGlua3NjYXBlOnBhZ2VzaGFkb3c9IjIiCiAgICAgaW5rc2NhcGU6d2luZG93LXdpZHRoPSIxODAwIgogICAgIGlua3NjYXBlOndpbmRvdy1oZWlnaHQ9IjEwNTkiCiAgICAgaWQ9Im5hbWVkdmlldzMwMzEiCiAgICAgc2hvd2dyaWQ9ImZhbHNlIgogICAgIGlua3NjYXBlOnpvb209IjAuMDkzMTIzNDM4IgogICAgIGlua3NjYXBlOmN4PSI5MDguNDE4NzIiCiAgICAgaW5rc2NhcGU6Y3k9IjcxNC4xMzg4NCIKICAgICBpbmtzY2FwZTp3aW5kb3cteD0iLTkiCiAgICAgaW5rc2NhcGU6d2luZG93LXk9Ii05IgogICAgIGlua3NjYXBlOndpbmRvdy1tYXhpbWl6ZWQ9IjEiCiAgICAgaW5rc2NhcGU6Y3VycmVudC1sYXllcj0ic3ZnMzAyNSIgLz4KICA8cGF0aAogICAgIHN0eWxlPSJmaWxsOmN1cnJlbnRDb2xvciIKICAgICBpbmtzY2FwZTpjb25uZWN0b3ItY3VydmF0dXJlPSIwIgogICAgIGlkPSJwYXRoMzAyOSIKICAgICBkPSJtIDE2MzguNTIxNyw5NCBxIDQwLDU3IDE4LDEyOSBsIC0yNzUsOTA2IHEgLTE5LDY0IC03Ni41LDEwNy41IC01Ny41LDQzLjUgLTEyMi41LDQzLjUgSCAyNTkuNTIxNzQgcSAtNzcsMCAtMTQ4LjUsLTUzLjUgUSAzOS41MjE3MzksMTE3MyAxMS41MjE3MzksMTA5NSBxIC0yNCwtNjcgLTEuOTk5OTk5OSwtMTI3IDAsLTQgMi45OTk5OTk5LC0yNyAzLC0yMyA0LC0zNyAxLC04IC0zLC0yMS41IC0zLjk5OTk5OTksLTEzLjUgLTMsLTE5LjUgMiwtMTEgOCwtMjEgNiwtMTAgMTYuNSwtMjMuNSAxMC41LC0xMy41IDE2LjUsLTIzLjUgMjMsLTM4IDQ1LC05MS41IDIyLjAwMDAwMSwtNTMuNSAzMC4wMDAwMDEsLTkxLjUgMywtMTAgMC41LC0zMCAtMi41LC0yMCAtMC41LC0yOCAzLC0xMSAxNywtMjggMTQsLTE3IDE3LC0yMyAyMSwtMzYgNDIsLTkyIDIxLC01NiAyNSwtOTAgMSwtOSAtMi41LC0zMiAtMy41LC0yMyAwLjUsLTI4IDQsLTEzIDIyLC0zMC41IDE4LC0xNy41IDIyLC0yMi41IDE5LC0yNiA0Mi41LC04NC41IDIzLjUsLTU4LjUgMjcuNSwtOTYuNSAxLC04IC0zLC0yNS41IC00LC0xNy41IC0yLC0yNi41IDIsLTggOSwtMTggNywtMTAgMTgsLTIzIDExLC0xMyAxNywtMjEgOCwtMTIgMTYuNSwtMzAuNSA4LjUsLTE4LjUgMTUsLTM1IDYuNSwtMTYuNSAxNiwtMzYgOS41LC0xOS41IDE5LjUsLTMyIDEwLC0xMi41IDI2LjUsLTIzLjUgMTYuNSwtMTEgMzYsLTExLjUgMTkuNSwtMC41IDQ3LjUsNS41IGwgLTEsMyBxIDM4LC05IDUxLC05IGggNzYwLjk5OTk2IHEgNzQsMCAxMTQsNTYgNDAsNTYgMTgsMTMwIGwgLTI3NCw5MDYgcSAtMzYsMTE5IC03MS41LDE1My41IC0zNS41LDM0LjUgLTEyOC41LDM0LjUgSCAxNTUuNTIxNzQgcSAtMjcsMCAtMzgsMTUgLTExLDE2IC0xLDQzIDI0LDcwIDE0NCw3MCBoIDkyMi45OTk5NiBxIDI5LDAgNTYsLTE1LjUgMjcsLTE1LjUgMzUsLTQxLjUgbCAzMDAsLTk4NyBxIDcsLTIyIDUsLTU3IDM4LDE1IDU5LDQzIHogTSA1NzQuNTIxNzQsOTYgcSAtNCwxMyAyLDIyLjUgNiw5LjUgMjAsOS41IGggNjA3Ljk5OTk2IHEgMTMsMCAyNS41LC05LjUgMTIuNSwtOS41IDE2LjUsLTIyLjUgbCAyMSwtNjQgcSA0LC0xMyAtMiwtMjIuNSAtNiwtOS41IC0yMCwtOS41IEggNjM3LjUyMTc0IHEgLTEzLDAgLTI1LjUsOS41IC0xMi41LDkuNSAtMTYuNSwyMi41IHogbSAtODMsMjU2IHEgLTQsMTMgMiwyMi41IDYsOS41IDIwLDkuNSBoIDYwNy45OTk5NiBxIDEzLDAgMjUuNSwtOS41IDEyLjUsLTkuNSAxNi41LC0yMi41IGwgMjEsLTY0IHEgNCwtMTMgLTIsLTIyLjUgLTYsLTkuNSAtMjAsLTkuNSBIIDU1NC41MjE3NCBxIC0xMywwIC0yNS41LDkuNSAtMTIuNSw5LjUgLTE2LjUsMjIuNSB6IiAvPgo8L3N2Zz4K
   :target: https://hirschheissich.gitlab.io/yourls-bot/
   :alt: Documentation

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code Style Black

.. image:: https://awesome.re/mentioned-badge.svg
   :target: https://github.com/YOURLS/awesome-yourls#3rd-party-integrations-and-frameworks--
   :alt: Mentioned in Awesome YOURLS


A Telegram bot that acts as GUI for the YOURLS API. Head `here <https://hirschheissich.gitlab.io/yourls-bot/userguide.html>`_ for the user guide.

Clarifications:
---------------

This project

* is *not* officially associated with `YOURLS <https://yourls.org>`_
* does *not* provide a ready to use Telegram Bot. You need to host your own instance.
* is *not* designed for large setups. More precisely it does not scale well for a large user base or YOURLS instances
  with great numbers of shortened URLS.

For details, please see below.

What this project does
----------------------

`YOURLS <https://yourls.org>`_ is an open source URL shortening service. This project allows to deploy a Telegram Bot which can be used to shorten links without having to open the corresponding website. For this to work, you need access to the `YOURLS API <https://yourls.org/#API>`_ of your instance.

Setting things up
-----------------

This project assumes that your YOURLS instance has the following plugins installed:

* `yourls-api-delete <https://github.com/claytondaley/yourls-api-delete>`_
* `this fork-branch <https://github.com/Bibo-Joshi/yourls-api-edit-url/tree/dev>`_ of the `yourls-api-edit-url <https://github.com/timcrockford/yourls-api-edit-url>`_ plugin

While only some of the bots functionality relies on those plugins, it is currently not possible to deactivate it.

Moreover, the bot assumes that the ``YOURLS_UNIQUE_URLS`` setting is ``false``, i.e. multiple short URLs can link to the
same long URL.

Once your YOURLS instance is ready, install the dependencies via ``pip install -r requirements.txt`` and fill the ``bot.ini`` configuration file:

* ``token``: your bots token
* ``admins_chat_id``: your Telegram ID. Used to send error reports
* ``client``: URL of your YOURLS instance. The ``/yourls-api.php`` suffix can be skipped
* ``signature``: Your signature for the YOURLS API.
* ``cache_timeout``: For some of the functionality it's necessary to get all short URLs currently stored on your YOURLS
  instance. This is done in a cached manner, i.e. the short URLS are retrieved at most every ``cache_timeout`` seconds.
  Defaults to 10 seconds.

That's it. Run the bot via ``python main.py``. 🤖

Note on backends
----------------

For the communication with the YOURLS API, this project uses this `fork-branch <https://github.com/Bibo-Joshi/yourls-python/tree/extensions>`_ of the `yourls-python <https://pypi.org/project/yourls/>`_ library.
