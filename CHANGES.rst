=========
Changelog
=========

Version 1.0.1
-------------
*Released 2021-01-12*

* Minor improvements in the ``add/kick_user`` dialogues

Version 1.0
-----------
*Released 2021-01-05*

Initial release.
