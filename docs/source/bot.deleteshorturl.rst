bot.delete_shorturl Module
==========================

.. automodule:: bot.delete_shorturl
    :members:
    :undoc-members:
    :show-inheritance: